# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0-rc46-4] - 2021-09-02
### Fixed
- SELinux mtail program is now able to parse SELinux messages from both journal and rsyslog

## [3.0.0-rc46-3] - 2021-08-25
### Changed
- documentation only

## [3.0.0-rc46-2] - 2021-08-19
### Added
- mtail programs (SELinux, nova-compute, OVN/OVS) now included in the container image
### Changed
- CI update to use docker dind latest downstream version

## [3.0.0-rc46-1] - 2021-06-24
### Changed
- documentation, release semver v2 suffix should be > 0, still on [google/mtailv3.0.0-rc46](https://github.com/google/mtail/releases/tag/v3.0.0-rc46)

## [3.0.0-rc46-0] - 2021-06-24
### Added
- Initial release, sticked to https://github.com/google/mtail/releases/tag/v3.0.0-rc46
