FROM centos:8

ARG VERSION=unknown-version
ARG BUILD_DATE=unknown-date
ARG CI_COMMIT_SHA=unknown
ARG CI_BUILD_HOSTNAME
ARG CI_BUILD_JOB_NAME
ARG CI_BUILD_ID
ARG MTAIL_VERSION
ARG MTAIL_BINARY_URL="https://github.com/google/mtail/releases/download/v${MTAIL_VERSION}/mtail_${MTAIL_VERSION}_Linux_x86_64.tar.gz"

COPY dependencies.yum.txt checksums.txt /tmp/


RUN yum -y update && \
    yum -y install $(cat /tmp/dependencies.yum.txt) && \
    curl -L "${MTAIL_BINARY_URL}" --output "/tmp/$(basename "${MTAIL_BINARY_URL}")" && \
    bash -c "cd /tmp && sha256sum --ignore-missing -c checksums.txt" && \
    tar xf "/tmp/$(basename "${MTAIL_BINARY_URL}")" --directory /usr/local/bin && \
    rm -f "/tmp/$(basename "${MTAIL_BINARY_URL}")" && \
    yum clean all && \
    mkdir -p /etc/mtail/programs

COPY programs/*.mtail /etc/mtail/programs/

ENTRYPOINT ["/usr/local/bin/mtail"]

LABEL maintainer="MetaCentrum Cloud Team <cloud[at]ics.muni.cz>" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Masaryk University, ICS" \
      org.label-schema.name="mtail" \
      org.label-schema.version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
      org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
      org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
      org.label-schema.url="https://gitlab.ics.muni.cz/cloud/mtail" \
      org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/mtail" \
      org.label-schema.vcs-ref="$CI_COMMIT_SHA"
