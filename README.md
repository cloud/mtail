# mtail

[google/mtail](https://github.com/google/mtail) - extract whitebox monitoring data from application logs for collection into a timeseries database

mtail is packaged as container `registry.gitlab.ics.muni.cz:443/cloud/mtail:latest` and comes with:
 * mtail binary
 * mtail programs in `/etc/mtail/programs` directory

## How to release new version
 1. [Check last upstream version](https://github.com/google/mtail/releases/).
 1. Update [CHANGELOG.md](/CHANGELOG.md) with same upstream version including `-<release-integer>` suffix to be able to release multiple versions based on same upstream code. (`<release-integer>` is integer > 0)
 1. Tag repo with that semver v2 compatible tag. (for instance [`v3.0.0-rc46-1`](https://gitlab.ics.muni.cz/cloud/mtail/-/tags/v3.0.0-rc46-1))
